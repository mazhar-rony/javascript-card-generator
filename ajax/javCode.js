let btnLoadData = document.getElementById('btnLoadData');
//btnLoadData.addEventListener('click', reqListener , false)

let countries = null;

// function reqListener() {
//     let url = 'https://restcountries.com/v3.1/all';
//     const xhr = new XMLHttpRequest();
//     xhr.open('GET', url);
//     xhr.send();
//     console.log(xhr)
//     xhr.onreadystatechange = () => {
//         if (xhr.status == 200 && xhr.readyState == 4) { // analyze HTTP status of the response
//             countries = xhr.responseText; // response is the server response
//             console.log(typeof countries);
//             JSON.parse(countries).forEach(country => {
//                 console.log(country.name.common);
//             });
//         } else { // show the result
//             console.log(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
//         }
//     }
// }

//console.log(countries);

//console.log(xhr);

//console.log(xhr);

//console.log(xhr);

// xhr.onload = function () {
//     if (xhr.status == 200 && xhr.readyState == 4) { // analyze HTTP status of the response
//         console.log(xhr.responseText); // response is the server response
//     } else { // show the result
//         console.log(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
//     }
// };

// xhr.onerror = function () {
//     alert("Request failed");
// };

//AXIOS

//const axios = require('axios');
//console.log(axios);
//Make a request for a user with a given ID
// axios.get('https://restcountries.com/v3.1/all')
//     .then(function (response) {
//         //handle success
//         response.data.forEach(country => {
//             console.log(country.name.common);
//     })
//     })
//     .catch(function (error) {
//         // handle error
//         console.log(error);
//     })
//     .then(function () {
//         // always display no matter whether it succeed or error
//         console.log("I m Ok");
//     });


//AXIOS FETCH

fetch('https://restcountries.com/v3.1/all')
    .then(response => response.json())
    .then(data => {
        data.forEach(country => {
            console.log(country.name.common);
        })
    });

//HW => Use Fetch/Axios to submit data using POST method, show the submitted data in console.
